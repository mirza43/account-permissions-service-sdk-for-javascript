## Description
Precor Connect account permissions service SDK for javascript.

## Features

#####
* [documentation](features/GetAccountPermissions.feature)

## Setup

**install via jspm**  
```shell
jspm install account-permissions-service-sdk=bitbucket:precorconnect/account-permissions-service-sdk-for-javascript
``` 

**import & instantiate**
```javascript
import AccountPermissionsServiceSdk,{AccountPermissionsServiceSdkConfig} from 'account-permissions-service-sdk'

const accountPermissionsServiceSdkConfig =
    new AccountPermissionsServiceSdkConfig(
        "https://api-dev.precorconnect.com"
    );
    
const accountPermissionsServiceSdk =
    new AccountPermissionsServiceSdk(
        accountPermissionsServiceSdkConfig
    );
```

## Platform Support

This library can be used in the **browser**.

## Develop

#### Software
- git
- npm

#### Scripts

install dependencies (perform prior to running or testing locally)
```PowerShell
npm install
```

unit & integration test in multiple browsers/platforms
```PowerShell
# note: following environment variables must be present:
# SAUCE_USERNAME
# SAUCE_ACCESS_KEY
npm test
```