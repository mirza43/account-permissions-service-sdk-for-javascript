import {inject} from 'aurelia-dependency-injection';
import AccountPermissionsServiceSdkConfig from './accountPermissionsServiceSdkConfig';
import {HttpClient} from 'aurelia-http-client'
import AccountPermissionsSynopsisView from './accountPermissionsSynopsisView';

@inject(AccountPermissionsServiceSdkConfig, HttpClient)
class GetAccountPermissionsWithAccountIdFeature {

    _config:AccountPermissionsServiceSdkConfig;

    _httpClient:HttpClient;

    constructor(config:AccountPermissionsServiceSdkConfig,
                httpClient:HttpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;

    }

    /**
     *
     * @param {string} accountId
     * @param {string} accessToken
     * @returns {Promise.<AccountPermissionsSynopsisView>}
     */
    execute(accountId:string,
            accessToken:string):Promise<AccountPermissionsSynopsisView> {

        return this._httpClient
            .createRequest(`account-permissions/${accountId}`)
            .asGet()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .send()
            .then(response => {if(response.response){ return response.content }else {return null}});
    }
}

export default GetAccountPermissionsWithAccountIdFeature;

